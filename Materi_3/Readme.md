# Sistem Grade Siswa

# Description
Sebuah aplikasi sederhana yang digunakan untuk melakukan input nilai siswa pada tiap mata pelajaran.

# Requirement
- PHP >= 5.0.0
- Apache Server

# Installation
- Pindahkan project file ini ke dalam htdocs,
- Kemudian skses menggunakan web browser.

# Usage
- Buka url 'http://localhost/nama-folder-project/' pada web
- Isi nama dan password untuk melakukan login
- Inputkan kelas dan data nilai.
- Klik tombol submit
- Aplikasi akan menghitung total nilai, rata-rata nilai, dan grade nilai.

# Credits 
[Bootstrap CSS Framework] (https://getbootstrap.com)
[Abimanyu Manusakerti] (https://gitlab.com/18102110)