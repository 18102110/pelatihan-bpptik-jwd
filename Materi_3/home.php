<?php
$daftar_kelas = ["Kelas-1", "Kelas-2", "Kelas-3"];
sort($daftar_kelas);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="assets/style/style_home.css">
    <title>Garding</title>
</head>

<body>
    <div class="row col">

        <div class="content1 col">
            <div class="content mt-5 col-md-12">
                <div class="judul text-center">
                    <h2>Sistem Grade Siswa</h2>
                </div>
                <form method="post">
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label for="nama">Nama</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $_GET['name']; ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label for="kelas">Kelas</label>
                        </div>
                        <div class="col-md-10">
                            <select class="form-control" name="kelas" id="inputGroupSelect01">
                                <option value="" disabled selected>Pilih Kelas</option>
                                <?php
                                foreach ($daftar_kelas as $kelas) {
                                    echo "<option value=" . $kelas . ">" . $kelas . "</option>";
                                }

                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="input mt-5 text-center">
                        <h2>Input Nilai Siswa</h2>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2">
                            <label for="nama">Matematika</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="mtk" name="mtk">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label for="nama">Fisika</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="fisika" name="fisika">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label for="nama">Biologi</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="biologi" name="biologi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label for="nama">TIK</label>
                        </div>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="tik" name="tik">
                        </div>
                    </div>
                    <div class="button text-right pb-3">
                        <button type="submit" class="btn btn-secondary" name="Submit" value="Submit">Submit</button>
                    </div>
                </form>
            </div>

        </div>
        <?php
        function total()
        {
            $mtk = $_POST['mtk'];
            $fisika = $_POST['fisika'];
            $biologi = $_POST['biologi'];
            $tik = $_POST['tik'];
            return ($mtk + $fisika + $biologi + $tik);
        }
        function rata()
        {
            $mtk = $_POST['mtk'];
            $fisika = $_POST['fisika'];
            $biologi = $_POST['biologi'];
            $tik = $_POST['tik'];
            return (($mtk + $fisika + $biologi + $tik) / 4);
        }
        function grade()
        {
            if (rata() >= 80) {
                return ('A');
            } elseif (rata() >= 60 && rata() <= 79) {
                return ('B');
            } elseif (rata() >= 50 && rata() <= 59) {
                return ('C');
            } else {
                return ('D');
            }
        }    

        if (isset($_POST['Submit'])) {

            //Variabel berisi path file data.json yang digunakan untuk menyimpan data nilai.
            $berkas = "data/data.json"; 

            //Mengambil data nilai dari file JSON
            $dataJson = file_get_contents($berkas);

            //Mengubah data nilai dalam format JSON ke dalam format array PHP.
            $data_nilai = json_decode($dataJson, true);

            //Variabel $data_nilai berisi data-data nilai dari form dalam bentuk array.
            $data_nilai[] = array(
                'Nama Siswa' => $_POST['name'],
                'Kelas' => $_POST['kelas'],
                'Nilai Matematika' => $_POST['mtk'],
                'Nilai Fisika' => $_POST['fisika'],
                'Nilai Biologi' => $_POST['biologi'],
                'Nilai TIK' => $_POST['tik'],
                'Total Nilai' => total(),
                'Rata-Rata' => rata(),
                'Grade Nilai' => grade()
            );

            //Mengubah data nilai yang berbentuk array PHP menjadi bentuk JSON.
            $dataJson = json_encode($data_nilai, JSON_PRETTY_PRINT);

            //Menyimpan data nilai yang berbentuk JSON ke dalam file JSON
            file_put_contents($berkas, $dataJson);
        }

        ?>
        <div class="content2 col">
            <div class="content mt-5 col-md-12">
                <div class="judul2 text-center">
                    <h2>Hasil Penilaian</h2>
                </div>
            </div>
            <div class="col">
                <label for="nama" name="name">Nama Siswa :
                    <?php
                    if (isset($_POST['name'])) {
                        echo $_POST['name'];
                    }
                    ?>

                </label>
            </div>
            <div class="col">
                <label for="kelas" name="kelas">Kelas :
                    <?php
                    if (isset($_POST['kelas'])) {
                        echo $_POST['kelas'];
                    }
                    ?>
                </label>
            </div>

            <h2></h2>
            <div class="col">
                <label for="mtk" name="mtk">Nilai Matematika :
                    <?php
                    if (isset($_POST['mtk'])) {
                        echo $_POST['mtk'];
                    }
                    ?>

                </label>
            </div>
            <div class="col">
                <label for="fisika" name="fisika">Nilai Fisika :
                    <?php
                    if (isset($_POST['fisika'])) {
                        echo $_POST['fisika'];
                    }
                    ?>
                </label>
            </div>
            <div class="col">
                <label for="biologi" name="biologi">Nilai Biologi :
                    <?php
                    if (isset($_POST['biologi'])) {
                        echo $_POST['biologi'];
                    }
                    ?>
                </label>
            </div>
            <div class="col">
                <label for="tik" name="tik">Nilai TIK :
                    <?php
                    if (isset($_POST['tik'])) {
                        echo $_POST['tik'];
                    }
                    ?>
                </label>
            </div>

            <h2></h2>
            <div class="col">
                <label for="rata" name="rata">Nilai Rata-Rata :
                    <?php
                    if (isset($_POST['Submit'])) {
                        echo rata();
                    }
                    ?>
                </label>
            </div>
            <div class="col">
                <label for="total" name="total">Total Nilai :
                    <?php
                    if (isset($_POST['Submit'])) {
                        echo total();
                    }
                    ?>
                </label>
            </div>
            <div class="col">
                <label for="grade" name="grade">Grade Nilai :
                    <?php
                    if (isset($_POST['Submit'])) {
                        echo grade();
                    }
                    ?>
                </label>
            </div>
        </div>

    </div>
</body>

</html>