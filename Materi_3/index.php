<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />

    <link rel="stylesheet" href="assets/style/style_login.css" />
    <title>Login</title>
</head>

<body style="background-color: #036ed1">
    <div class="pt-5">
        <div class="card container pt-5 rounded shadow justify-content-center" style="width: 60rem; height: 35rem; background-color: white">
            <div class="row card-body text-center">
                <div class="col-md-7 pt-3">
                    <img class="rounded pt-3" src="assets/images/login.jpg" style="width: 30rem; height: 23rem;" alt="">
                </div>
                <div class="col-md-5">
                    <div class="align-self-end">
                        <div class="col-md-12 align-self-end">
                            <h2 class="card-title fw-bold">Login</h2>
                            <form action="home.php" method="get">
                                <div class="form-group mb-3 pt-4">
                                    <input type="text" class="form-control rounded" name="name" placeholder="Name" autofocus />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control rounded" name="password" placeholder="Password" />
                                </div>
                                <div class="form-group pt-5">
                                    <button type="submit" class="btn btn-primary rounded" style="width: 100%;">LOGIN</button>
                                </div>
                                <div class="text-right pb-3">
                                    <a href="#" class="text-primary">Lupa Password?</a>
                                </div>
                                <hr>
                                <div class="text-left">
                                    <p>Tidak Punya Akun?<a href="#" class="text-primary"> Daftar Disini</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>